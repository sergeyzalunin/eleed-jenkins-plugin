package org.jenkinsci.plugins;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Global {


    public static Process StartCMS(String eleedDirectory) throws IOException, InterruptedException {
        String consolePath = Paths.get(eleedDirectory,"ConsoleMonolithicServer.exe").toString();
        Path consoleLogPath = Paths.get(eleedDirectory,"\\Logs\\ConsoleMonolithic.log");

        if (Files.exists(consoleLogPath)) {
            PrintWriter writer = new PrintWriter(consoleLogPath.toString());
            writer.print("");
            writer.close();
        }
        Process cms = Runtime.getRuntime().exec(consolePath);
        if (!Files.exists(consoleLogPath)) Thread.sleep(1000);
        while(!readFile(consoleLogPath, Charset.defaultCharset()).contains("DeleterPlugin plugin is loaded")) {
            //TODO: сделать какую-нибудь херню. Пока запускается ConsoleMonolithicServer
            Thread.sleep(500);
        }

        return cms;
    }

    private static String readFile(Path path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(path);
        return new String(encoded, encoding);
    }

    public static String GetParsedBuffer(BufferedReader reader) throws IOException {
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();
        return result;
    }
}

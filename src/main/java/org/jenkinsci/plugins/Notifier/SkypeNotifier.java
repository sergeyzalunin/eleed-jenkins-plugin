package org.jenkinsci.plugins.Notifier;

import hudson.Extension;
import hudson.model.*;
import hudson.model.listeners.RunListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;

public class SkypeNotifier extends Notifier {

    @DataBoundConstructor
    public SkypeNotifier() {

    }

    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return null;
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Publisher> {
        // Publisher because Notifiers are a type of publisher
        private String global;

        @Override
        public String getDisplayName() {
            return "Eleed skype notifier"; // What people will see as the plugin name in the configs
        }

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return false;
        }
        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws Descriptor.FormException {
            global = formData.getString("global");
            save();
            return super.configure(req, formData);
        }

    }

    @Extension
    public static class BuildListener extends RunListener<AbstractBuild> {

        @Override
        public void onStarted(AbstractBuild abstractBuild, TaskListener listener) {
            // Do nothing.
        }

        @Override
        public void onCompleted(AbstractBuild abstractBuild, TaskListener listener) {

           if (abstractBuild.getResult() == Result.FAILURE) {

           }

            //new BuildmetricsNotifier().onCompleted(abstractBuild, listener);
           /* if (abstractBuild.getResult() == Result.FAILURE){

                Skype skype = new SkypeBuilder("Skype4JGuest").withChat("https://join.skype.com/olIeK6fzY2pc").withAllResources().build();

           if (abstractBuild.getResult() == Result.FAILURE) {


           }*/
        }
    }
}

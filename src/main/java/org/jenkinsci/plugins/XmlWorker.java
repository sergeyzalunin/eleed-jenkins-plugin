package org.jenkinsci.plugins;

import hudson.scm.SCMRevisionState;
import org.jenkinsci.plugins.SCM.EleedSCMRevisionState;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XmlWorker {

    private final static String eleedSCMStateFileName = "eleedscmstate.xml";
    private final static String baselineFileName = "baseline.xml";

    public static EleedSCMRevisionState readEleedSCMRevisionStateFromXml(String directory) {

        Path baselinePath = getEleedScmStatePath(directory);

        EleedSCMRevisionState result = getEleedSCMRevisionState(baselinePath);

        return result;
    }

    public static EleedSCMRevisionState readBaselineFromXml(String directory) {

        Path baselinePath = getBaselinePath(directory);

        EleedSCMRevisionState result = getEleedSCMRevisionState(baselinePath);

        return result;
    }

    public static void writeEleedSCMRevisionStateXml(SCMRevisionState currentSCMState, String directory){

        Path baselinePath = getEleedScmStatePath(directory);
        writeSerializeData(currentSCMState, baselinePath);
    }

    public static void writeBaselineSerializationXml(SCMRevisionState currentSCMState, String directory){

        Path baselinePath = getBaselinePath(directory);
        writeSerializeData(currentSCMState, baselinePath);
    }

    public static Path getEleedScmStatePath(String directory) {

        Path result = Paths.get(directory, eleedSCMStateFileName);
        return result;
    }

    public static Path getBaselinePath(String directory) {

        Path result = Paths.get(directory, baselineFileName);
        return result;
    }

    private static EleedSCMRevisionState getEleedSCMRevisionState(Path baselinePath) {

        EleedSCMRevisionState result = null;

        if (Files.exists(baselinePath)) {

            Persister serializer = new Persister();
            try {
                result = serializer.read(EleedSCMRevisionState.class, baselinePath.toFile(), false);
            } catch (Exception e) {
                result = new EleedSCMRevisionState();
                e.printStackTrace();
            }
        }

        return result;
    }

    private static void writeSerializeData(SCMRevisionState currentSCMState, Path baselinePath) {
        Serializer serializer = new Persister();
        try {
            serializer.write(currentSCMState, baselinePath.toFile());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

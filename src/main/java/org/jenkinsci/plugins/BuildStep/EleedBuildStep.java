package org.jenkinsci.plugins.BuildStep;

import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.model.AbstractProject;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import hudson.triggers.SCMTrigger;
import jenkins.tasks.SimpleBuildStep;
import org.jenkinsci.plugins.EleedServiceClient;
import org.jenkinsci.plugins.Global;
import org.jenkinsci.plugins.SCM.EleedReplicInfo;
import org.jenkinsci.plugins.SCM.EleedSCMRevisionState;
import org.jenkinsci.plugins.XmlWorker;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

public class EleedBuildStep extends Builder implements SimpleBuildStep {

    private String eleedDirectory;
    private String login;
    private String password;

    @DataBoundConstructor
    public EleedBuildStep(String eleedDirectory, String login, String password) {
        this.eleedDirectory = eleedDirectory;
        this.login = login;
        this.password = password;
    }

    public String getEleedDirectory() {
        return eleedDirectory;
    }


    public String getLogin() {
        return login;
    }

    @DataBoundSetter
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    @DataBoundSetter
    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public void perform(@Nonnull Run<?, ?> run, @Nonnull FilePath filePath,
                        @Nonnull Launcher launcher, @Nonnull TaskListener taskListener)
                    throws InterruptedException, IOException {

        if (run.getCauses().get(0).getClass() == SCMTrigger.SCMTriggerCause.class) {

            EleedSCMRevisionState scmState = XmlWorker.readEleedSCMRevisionStateFromXml(run.getRootDir().getPath());

            Process cms = null;
            try {
                EleedReplicInfo replicInfo = scmState.getReplicInfo();
                if (replicInfo == null || replicInfo.getReplicId() <= 0)
                    taskListener.getLogger().println("Не удалось загрузить информацию о реплике");
                else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                    taskListener.getLogger().println(
                            String.format("\nЗагрузка реплики %1$s.%2$s;\n" +
                                            "Версия платформы - %3$s;\n" +
                                            "Дата выгрузки - %4$s;\n" +
                                            "Разработчик - %5$s;\n" +
                                            "Комментарий - %6$s;\n",
                                    replicInfo.getIterationNumber(),
                                    replicInfo.getReplicId(),
                                    replicInfo.getPlatformVersion(),
                                    dateFormat.format(replicInfo.getDateLoaded().getTime()),
                                    replicInfo.getUserName(),
                                    replicInfo.getComment()));

                    taskListener.getLogger().println("Запуск ConsoleMonolithicServer.exe");

                    cms = Global.StartCMS(this.eleedDirectory);

                    taskListener.getLogger().println("Выгрузка реплики из системы контроля реплик");
                    byte[] replicData = EleedServiceClient.GetReplicData(EleedServiceClient.getLastStub(), replicInfo.getReplicId());
                    String replicPath = writeFile(replicData, replicInfo.getReplicId(), run.getRootDir().getPath());

                    LoadReplic(replicPath, taskListener);
                    Files.delete(Paths.get(replicPath));

                    Compile(taskListener);
                }
            } finally {
                if (cms != null)
                    cms.destroy();
            }
        }
    }

    private String writeFile(byte[] replicData, long replicId, String directory) throws IOException {
        //create a temp file
        File temp = Paths.get(directory,"replic" + replicId + ".rep").toFile();

        //write it
        FileOutputStream  bw = new FileOutputStream (temp);
        bw.write(replicData);
        bw.close();

        return temp.getPath();
    }

    private void LoadReplic(String replicPath, TaskListener taskListener) throws IOException, InterruptedException {

        taskListener.getLogger().println("Загрузка реплики на тестовый сервер");

        String adminToolsPath = Paths.get(eleedDirectory,"Akforta.eLeed.AdminToolsConsole.exe").toString();

        ProcessBuilder builder = new ProcessBuilder(adminToolsPath,
                "--plugin", "\"InnerReplicationPlugin\"", "--user", "\""+login+"\"", "--password", "\""+password+"\"",
                        "--import", "--nocompilation", "--file", "\""+replicPath+"\"");
        builder.directory(new File(eleedDirectory));
        builder.redirectErrorStream(true);
        Process p =  builder.start();

        int exitcode = p.waitFor();

        String stdout = Global.GetParsedBuffer(new BufferedReader(new InputStreamReader(p.getInputStream(), "CP866")));
        taskListener.getLogger().println(stdout);

        if (exitcode != 1)
            throw new InvalidObjectException("Ошибка при загрузке реплики");
    }

    private void Compile(TaskListener taskListener) throws IOException, InterruptedException {

        taskListener.getLogger().println("Компиляция реплики на тестовом сервере");

        String adminToolsPath = Paths.get(eleedDirectory,"BIZ.Compiler.exe").toString();
        String command = String.format("%1$s --user \"%2$s\" " +" --password  \"%3$s\"",
                adminToolsPath, login, password);

        ProcessBuilder builder = new ProcessBuilder(adminToolsPath, "--user", "\"" + login + "\"", "--password", "\""+ password+"\"");
        builder.directory(new File(eleedDirectory));
        builder.redirectErrorStream(true);
        Process p =  builder.start();

        int exitcode = p.waitFor();

        String stdout = Global.GetParsedBuffer(new BufferedReader(new InputStreamReader(p.getInputStream(), "CP866")));
        taskListener.getLogger().println(stdout);

        if (exitcode != 0)
            throw new InvalidObjectException("Ошибка компиляции eleed");
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Загрузка реплики в Eleed проект";
        }

    }
}

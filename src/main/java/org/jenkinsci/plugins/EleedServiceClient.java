package org.jenkinsci.plugins;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.jenkinsci.plugins.SCM.AdminReplicServiceStub;

import javax.activation.DataHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Calendar;

public class EleedServiceClient {

    private static AdminReplicServiceStub lastStub;

    private static AdminReplicServiceStub.ReplicInfoDataContract lastReplicInfo;

    public static AdminReplicServiceStub getLastStub() {
        return lastStub;
    }

    public static AdminReplicServiceStub.ReplicInfoDataContract getLastReplicInfo() {
        return lastReplicInfo;
    }

    public static AdminReplicServiceStub GetReplicServiceStub(String uri, String login, String pass) throws AxisFault {

        AdminReplicServiceStub stub = new AdminReplicServiceStub(uri);

        HttpTransportPropertiesImpl.Authenticator  basicAuth = new HttpTransportPropertiesImpl.Authenticator();
        basicAuth.setUsername(login);
        basicAuth.setPassword(pass);
        basicAuth.setPreemptiveAuthentication(true);
        final Options clientOptions = stub._getServiceClient().getOptions();
        clientOptions.setProperty(HTTPConstants.AUTHENTICATE, basicAuth);

        lastStub = stub;

        return stub;
    }

    public static AdminReplicServiceStub.ReplicInfoDataContract GetNextReplicaInfo(
                 AdminReplicServiceStub stub,
                 Calendar beginDate) throws RemoteException {

        if (stub == null) return null;

        AdminReplicServiceStub.ReplicInfoDataContract[] replicArray = GetAllReplicsFromDate(stub, beginDate);

        AdminReplicServiceStub.ReplicInfoDataContract result = null;
        if (replicArray != null && replicArray.length > 0)
            result = replicArray[0];
        lastReplicInfo = result;
        return result;
    }

    public static AdminReplicServiceStub.ReplicInfoDataContract GetLastReplicaInfo(
            AdminReplicServiceStub stub,
            Calendar beginDate) throws RemoteException {

        if (stub == null) return null;

        AdminReplicServiceStub.ReplicInfoDataContract[] replicArray = GetAllReplicsFromDate(stub, beginDate);

        AdminReplicServiceStub.ReplicInfoDataContract result = null;
        if (replicArray.length > 0)
            result = replicArray[replicArray.length-1];
        lastReplicInfo = result;
        return result;
    }

    private static AdminReplicServiceStub.ReplicInfoDataContract[] GetAllReplicsFromDate(
            AdminReplicServiceStub stub,
            Calendar beginDate) throws RemoteException {

        if (stub == null) return null;

        Calendar endDate = Calendar.getInstance();

        AdminReplicServiceStub.GetReplicsInfo param = new AdminReplicServiceStub.GetReplicsInfo();
        beginDate.add(Calendar.MILLISECOND,1);
        param.setDateFrom(beginDate);
        param.setDateTo(endDate);

        AdminReplicServiceStub.GetReplicsInfoResponse response = stub.getReplicsInfo(param);
        AdminReplicServiceStub.ArrayOfReplicInfoDataContract resultDataContract = response.getGetReplicsInfoResult();

        AdminReplicServiceStub.ReplicInfoDataContract[] replicArray = resultDataContract.getReplicInfoDataContract();

        return replicArray;
    }

    public static byte[] GetReplicData(AdminReplicServiceStub stub, long replicId) throws IOException {

        AdminReplicServiceStub.GetReplicData param = new AdminReplicServiceStub.GetReplicData();
        param.setReplicId(replicId);

        AdminReplicServiceStub.GetReplicDataResponse replicData = stub.getReplicData(param);
        DataHandler dataHandler = replicData.getGetReplicDataResult();

        return toBytes(dataHandler);
    }

    private static byte[] toBytes(DataHandler handler) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        handler.writeTo(output);
        return output.toByteArray();
    }
}

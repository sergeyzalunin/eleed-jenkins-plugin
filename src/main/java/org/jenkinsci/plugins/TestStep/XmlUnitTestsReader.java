package org.jenkinsci.plugins.TestStep;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class XmlUnitTestsReader {

    private final static String reportFileName = "NUnitScriptTestReport.xml";

    public List<EleedTestSuite> ParseDOM(String eleedDirectory) {

        List<EleedTestSuite> suiteList = new ArrayList<EleedTestSuite>();

        String filePath = Paths.get(eleedDirectory,reportFileName).toString();
        File xmlFile = new File(filePath);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("test-case");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node testCaseNode = nodeList.item(i);
                EleedTestSuite suite = getSuite(suiteList, testCaseNode.getParentNode());
                if (suite == null) continue;
                EleedTestCase testCase = createTestCase((Element) testCaseNode, suite.getSuite());
                suite.getCaseList().add(testCase);
            }

        } catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }

        return suiteList;
    }

    private EleedTestSuite getSuite(List<EleedTestSuite> suiteList, Node node) {

        if (node == null || node.getParentNode() == null) return null;
        Element parentNode = (Element)node.getParentNode();
        if (parentNode == null || parentNode.getNodeType() != Node.ELEMENT_NODE) return null;

        String parentName = getAttributeValue("name", parentNode);
        for(EleedTestSuite rec : suiteList) {
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                if (rec.getSuite().getName() == parentName)
                    return rec;
            }
        }

        EleedTestSuite result = createSuite(parentNode, parentName);
        suiteList.add(result);
        return result;
    }

    private EleedTestSuite createSuite(Element parentNode, String parentName) {

        EleedTestCase suiteCase = new EleedTestCase();
        suiteCase.setName(parentName);
        suiteCase.setSuccess(Boolean.parseBoolean(getAttributeValue("success", parentNode)));
        suiteCase.setTime(Double.parseDouble(getAttributeValue("time", parentNode)));
        suiteCase.setAsserts(Integer.parseInt(getAttributeValue("asserts", parentNode)));

        return new EleedTestSuite(suiteCase);
    }

    private EleedTestCase createTestCase(Element element, EleedTestCase suite) {

        EleedTestCase testCase = new EleedTestCase();
        testCase.setName(getAttributeValue("name", element));
        testCase.setExecuted(Boolean.parseBoolean(getAttributeValue("executed", element)));
        testCase.setSuccess(Boolean.parseBoolean(getAttributeValue("success", element)));
        testCase.setTime(Double.parseDouble(getAttributeValue("time", element)));
        testCase.setAsserts(Integer.parseInt(getAttributeValue("asserts", element)));
        setInnerMessageInfo(element, testCase);
        return testCase;
    }

    private void setInnerMessageInfo(Node testCaseNode, EleedTestCase testCase) {
        NodeList nodeList = testCaseNode.getChildNodes();
        if (nodeList == null || nodeList.getLength() < 2)
            return;
        Node failureChild = nodeList.item(1);
        if (failureChild == null)
            return;

        NodeList childNodes = failureChild.getChildNodes();
        if (childNodes != null) {
            if (childNodes.getLength() > 1)
                testCase.setMessage(childNodes.item(1).getTextContent());
            if (childNodes.getLength() > 2)
                testCase.setStackTrace(childNodes.item(3).getTextContent());
        }
    }

    private static String getAttributeValue(String attribute, Element element) {
        return element.getAttribute(attribute);
    }
}

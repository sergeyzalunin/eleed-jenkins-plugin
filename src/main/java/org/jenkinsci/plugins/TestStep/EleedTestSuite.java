package org.jenkinsci.plugins.TestStep;

import java.util.ArrayList;
import java.util.List;

public class EleedTestSuite {

    private EleedTestCase _suite;
    private List<EleedTestCase> _caseList;

    public EleedTestSuite(EleedTestCase _suite) {
        this._suite = _suite;
        _caseList = new ArrayList<EleedTestCase>();
    }

    public EleedTestCase getSuite() {
        return _suite;
    }

    public void setSuite(EleedTestCase _suite) {
        this._suite = _suite;
    }

    public List<EleedTestCase> getCaseList() {
        return _caseList;
    }

    public void setCaseList(List<EleedTestCase> _caseList) {
        this._caseList = _caseList;
    }
}

package org.jenkinsci.plugins.TestStep;

import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.model.AbstractProject;
import hudson.model.Result;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import jenkins.tasks.SimpleBuildStep;
import org.jenkinsci.plugins.Global;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.List;

public class EleedTestStep extends Builder implements SimpleBuildStep {

    private String eleedDirectory;
    private String login;
    private String password;
    private String ignoreTests;

    @DataBoundConstructor
    public EleedTestStep(String eleedDirectory, String login, String password, String ignoreTests) {
        this.eleedDirectory = eleedDirectory;
        this.login = login;
        this.password = password;
        this.ignoreTests = ignoreTests;
    }

    public String getEleedDirectory() {
        return eleedDirectory;
    }

    @DataBoundSetter
    public void setEleedDirectory(String eleedDirectory) {
        this.eleedDirectory = eleedDirectory;
    }

    public String getLogin() {
        return login;
    }

    @DataBoundSetter
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    @DataBoundSetter
    public void setPassword(String password) {
        this.password = password;
    }

    public String getIgnoreTests() { return ignoreTests; }

    @DataBoundSetter
    public void setIgnoreTests(String ignoreTests) {
        this.ignoreTests = ignoreTests;
    }

    @Override
    public void perform(@Nonnull Run<?, ?> run,
                        @Nonnull FilePath workspace,
                        @Nonnull Launcher launcher,
                        @Nonnull TaskListener listener) throws InterruptedException, IOException {

        Result lastBuildResult = run.getResult();
       if (lastBuildResult == null || lastBuildResult == Result.SUCCESS) {

            prepareToRunTests(listener);
            Process cms = null;
            try {
                listener.getLogger().println("Запуск ConsoleMonolithicServer.exe");
                cms = Global.StartCMS(this.eleedDirectory);
                startTestingApplication(run, listener);
                readUnitTestsXmlFile(run, listener);
            }
            finally {
                if (cms != null)
                    cms.destroy();
            }
        }
        else {
            listener.getLogger().println(
                    String.format("Последняя сборка была не удачна - %1$s. Тесты не будут запущены.", run.getResult()));
        }
    }

    private void prepareToRunTests(@Nonnull TaskListener listener) {
        listener.getLogger().println("Запуск тестов");

        if (this.ignoreTests != "") {
            listener.getLogger().println(
                    String.format("Проигнорированы будут следующие тесты - %1$s.", this.ignoreTests));
        }

    }

    private void startTestingApplication(@Nonnull Run<?, ?> run,
                                         @Nonnull TaskListener listener) throws IOException, InterruptedException {

        if (run.getResult() != null && run.getResult() != Result.SUCCESS) return;

        listener.getLogger().println("Запуск eLeedUnitTestApplication.exe на тестовом сервере");

        String eleedUnitTestApp = Paths.get(eleedDirectory,"eLeedUnitTestApplication.exe").toString();

        ProcessBuilder builder = new ProcessBuilder(eleedUnitTestApp,
                                                    "--user",
                                                    "\"" + login + "\"",
                                                    "--password",
                                                    "\""+ password+"\"");
        builder.directory(new File(eleedDirectory));
        builder.redirectErrorStream(true);
        Process p =  builder.start();

        int exitcode = p.waitFor();

        String stdout = Global.GetParsedBuffer(new BufferedReader(new InputStreamReader(p.getInputStream(), "CP866")));
        listener.getLogger().println(stdout);

        if (exitcode != 0) {
            listener.getLogger().println("Ошибка при запуске тестов eleed");
            run.setResult(Result.FAILURE);
        }
    }

    private void readUnitTestsXmlFile(@Nonnull Run<?, ?> run,
                                      @Nonnull TaskListener listener) {

        if (run.getResult() != null && run.getResult() != Result.SUCCESS) return;
        listener.getLogger().println("Обработка результатов тестов");

        XmlUnitTestsReader reader = new XmlUnitTestsReader();
        List<EleedTestSuite> testSuites = reader.ParseDOM(this.eleedDirectory);
        String[] ignoreTestList = this.ignoreTests.split(",");

        boolean allTestPassed = true;
        for(EleedTestSuite suite : testSuites) {
            if (skipTest(ignoreTestList, suite) || suite.getSuite().isSuccess()) continue;
            listener.getLogger().println(repeatedChars('-', 100));
            listener.getLogger().println("Test-suite:\t" + suite.getSuite().toString());

            listener.getLogger().println("Test-cases:");
            int i = 1;
            for (EleedTestCase testCase: suite.getCaseList()){
                if (!testCase.isSuccess()) {
                    listener.getLogger().println((i++) + ")" + testCase.toString());
                    run.setResult(Result.FAILURE);
                    allTestPassed = false;
                }
            }
        }

        if (allTestPassed)
            listener.getLogger().println("Все тесты удачно прошли");
    }

    private boolean skipTest(String[] ignoreTestList, EleedTestSuite suite) {

        for(String testName : ignoreTestList)
            if (testName.trim().equals(suite.getSuite().getName()))
                return true;
        return false;
    }

    private String repeatedChars(char value, int count) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < count; i++)
            buffer.append(value);
        return buffer.toString();
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Запуск тестов для проекта Eleed";
        }

    }
}

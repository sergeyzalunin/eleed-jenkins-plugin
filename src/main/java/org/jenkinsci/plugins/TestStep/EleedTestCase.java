package org.jenkinsci.plugins.TestStep;

public class EleedTestCase {

    private String _name;
    private boolean _executed;
    private boolean _success;
    private double _time;
    private int _asserts;
    private String _message;
    private String _stackTrace;

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public boolean isExecuted() {
        return _executed;
    }

    public void setExecuted(boolean _executed) {
        this._executed = _executed;
    }

    public boolean isSuccess() {
        return _success;
    }

    public void setSuccess(boolean _success) {
        this._success = _success;
    }

    public double getTime() {
        return _time;
    }

    public void setTime(double _time) {
        this._time = _time;
    }

    public int getAsserts() {
        return _asserts;
    }

    public void setAsserts(int _asserts) {
        this._asserts = _asserts;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(String _message) { this._message = _message; }

    public String getStackTrace() { return _stackTrace; }

    public void setStackTrace(String _stackTrace) { this._stackTrace = _stackTrace; }

    @Override
    public String toString() {
        String result = String.format("Name - %s;\t"+
                        "Executed - %b;\t"+
                        "Success - %b;\t"+
                        "Time - %f;\t"+
                        "Asserts - %d;",
        this._name, this._executed, this._success, this._time, this._asserts);

        if ((this._message != null && this._message.trim() != "") || (this._stackTrace != null && this._stackTrace.trim() != ""))
            result += String.format("\nMessage - %s;\n"+
                            "Stack trace: \n%s",
                    this._message, this._stackTrace);

        return result;
    }
}

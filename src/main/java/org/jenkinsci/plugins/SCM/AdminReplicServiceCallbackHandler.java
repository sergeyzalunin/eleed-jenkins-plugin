/**
 * AdminReplicServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.6  Built on : Jul 30, 2017 (09:08:31 BST)
 */
package org.jenkinsci.plugins.SCM;


/**
 *  AdminReplicServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class AdminReplicServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public AdminReplicServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public AdminReplicServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for getReplicData method
     * override this method for handling normal response from getReplicData operation
     */
    public void receiveResultgetReplicData(
        AdminReplicServiceStub.GetReplicDataResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getReplicData operation
     */
    public void receiveErrorgetReplicData(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAllObjects method
     * override this method for handling normal response from getAllObjects operation
     */
    public void receiveResultgetAllObjects(
        AdminReplicServiceStub.GetAllObjectsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllObjects operation
     */
    public void receiveErrorgetAllObjects(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getIssueSummary method
     * override this method for handling normal response from getIssueSummary operation
     */
    public void receiveResultgetIssueSummary(
        AdminReplicServiceStub.GetIssueSummaryResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getIssueSummary operation
     */
    public void receiveErrorgetIssueSummary(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for createFullReplic method
     * override this method for handling normal response from createFullReplic operation
     */
    public void receiveResultcreateFullReplic(
        AdminReplicServiceStub.CreateFullReplicResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from createFullReplic operation
     */
    public void receiveErrorcreateFullReplic(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getCentralReplicInfo method
     * override this method for handling normal response from getCentralReplicInfo operation
     */
    public void receiveResultgetCentralReplicInfo(
        AdminReplicServiceStub.GetCentralReplicInfoResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getCentralReplicInfo operation
     */
    public void receiveErrorgetCentralReplicInfo(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAllUsers method
     * override this method for handling normal response from getAllUsers operation
     */
    public void receiveResultgetAllUsers(
        AdminReplicServiceStub.GetAllUsersResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllUsers operation
     */
    public void receiveErrorgetAllUsers(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for beginNewIteration method
     * override this method for handling normal response from beginNewIteration operation
     */
    public void receiveResultbeginNewIteration(
        AdminReplicServiceStub.BeginNewIterationResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from beginNewIteration operation
     */
    public void receiveErrorbeginNewIteration(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getIssueTrackerUserName method
     * override this method for handling normal response from getIssueTrackerUserName operation
     */
    public void receiveResultgetIssueTrackerUserName(
         AdminReplicServiceStub.GetIssueTrackerUserNameResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getIssueTrackerUserName operation
     */
    public void receiveErrorgetIssueTrackerUserName(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for loginExists method
     * override this method for handling normal response from loginExists operation
     */
    public void receiveResultloginExists(
         AdminReplicServiceStub.LoginExistsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from loginExists operation
     */
    public void receiveErrorloginExists(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAllIterations method
     * override this method for handling normal response from getAllIterations operation
     */
    public void receiveResultgetAllIterations(
         AdminReplicServiceStub.GetAllIterationsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllIterations operation
     */
    public void receiveErrorgetAllIterations(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getPartialReplic method
     * override this method for handling normal response from getPartialReplic operation
     */
    public void receiveResultgetPartialReplic(
         AdminReplicServiceStub.GetPartialReplicResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getPartialReplic operation
     */
    public void receiveErrorgetPartialReplic(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getReplicsInfo method
     * override this method for handling normal response from getReplicsInfo operation
     */
    public void receiveResultgetReplicsInfo(
         AdminReplicServiceStub.GetReplicsInfoResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getReplicsInfo operation
     */
    public void receiveErrorgetReplicsInfo(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateUsers method
     * override this method for handling normal response from updateUsers operation
     */
    public void receiveResultupdateUsers(
         AdminReplicServiceStub.UpdateUsersResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateUsers operation
     */
    public void receiveErrorupdateUsers(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateMainDbToVersion method
     * override this method for handling normal response from updateMainDbToVersion operation
     */
    public void receiveResultupdateMainDbToVersion(
         AdminReplicServiceStub.UpdateMainDbToVersionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateMainDbToVersion operation
     */
    public void receiveErrorupdateMainDbToVersion(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for deleteObjects method
     * override this method for handling normal response from deleteObjects operation
     */
    public void receiveResultdeleteObjects(
         AdminReplicServiceStub.DeleteObjectsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from deleteObjects operation
     */
    public void receiveErrordeleteObjects(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getIterationInfoById method
     * override this method for handling normal response from getIterationInfoById operation
     */
    public void receiveResultgetIterationInfoById(
         AdminReplicServiceStub.GetIterationInfoByIdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getIterationInfoById operation
     */
    public void receiveErrorgetIterationInfoById(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getFullReplic method
     * override this method for handling normal response from getFullReplic operation
     */
    public void receiveResultgetFullReplic(
         AdminReplicServiceStub.GetFullReplicResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getFullReplic operation
     */
    public void receiveErrorgetFullReplic(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for changeObjectsMarking method
     * override this method for handling normal response from changeObjectsMarking operation
     */
    public void receiveResultchangeObjectsMarking(
         AdminReplicServiceStub.ChangeObjectsMarkingResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from changeObjectsMarking operation
     */
    public void receiveErrorchangeObjectsMarking(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateIterationName method
     * override this method for handling normal response from updateIterationName operation
     */
    public void receiveResultupdateIterationName(
         AdminReplicServiceStub.UpdateIterationNameResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateIterationName operation
     */
    public void receiveErrorupdateIterationName(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for isInitialFullReplicExists method
     * override this method for handling normal response from isInitialFullReplicExists operation
     */
    public void receiveResultisInitialFullReplicExists(
         AdminReplicServiceStub.IsInitialFullReplicExistsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from isInitialFullReplicExists operation
     */
    public void receiveErrorisInitialFullReplicExists(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for login method
     * override this method for handling normal response from login operation
     */
    public void receiveResultlogin(
         AdminReplicServiceStub.LoginResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from login operation
     */
    public void receiveErrorlogin(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateMainDbToLatestVersion method
     * override this method for handling normal response from updateMainDbToLatestVersion operation
     */
    public void receiveResultupdateMainDbToLatestVersion(
         AdminReplicServiceStub.UpdateMainDbToLatestVersionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateMainDbToLatestVersion operation
     */
    public void receiveErrorupdateMainDbToLatestVersion(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for isMainDbHasLatestVersion method
     * override this method for handling normal response from isMainDbHasLatestVersion operation
     */
    public void receiveResultisMainDbHasLatestVersion(
         AdminReplicServiceStub.IsMainDbHasLatestVersionResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from isMainDbHasLatestVersion operation
     */
    public void receiveErrorisMainDbHasLatestVersion(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for saveInitialFullReplic method
     * override this method for handling normal response from saveInitialFullReplic operation
     */
    public void receiveResultsaveInitialFullReplic(
         AdminReplicServiceStub.SaveInitialFullReplicResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from saveInitialFullReplic operation
     */
    public void receiveErrorsaveInitialFullReplic(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getReplicsInfoByObjects method
     * override this method for handling normal response from getReplicsInfoByObjects operation
     */
    public void receiveResultgetReplicsInfoByObjects(
         AdminReplicServiceStub.GetReplicsInfoByObjectsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getReplicsInfoByObjects operation
     */
    public void receiveErrorgetReplicsInfoByObjects(java.lang.Exception e) {
    }
}

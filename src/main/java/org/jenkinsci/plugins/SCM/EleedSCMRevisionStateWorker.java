package org.jenkinsci.plugins.SCM;

import hudson.scm.SCMRevisionState;
import org.jenkinsci.plugins.EleedServiceClient;

import java.rmi.RemoteException;

public class EleedSCMRevisionStateWorker extends SCMRevisionState {

    private EleedSCMRevisionState currentSCMState;
    private AdminReplicServiceStub.ReplicInfoDataContract replicInfo;

    public EleedSCMRevisionState getCurrentSCMState() { return currentSCMState; }

    public AdminReplicServiceStub.ReplicInfoDataContract getReplicInfo() {
        return replicInfo;
    }

    public boolean isNewerThan(EleedSCMRevisionState baseline, AdminReplicServiceStub stub) throws RemoteException {

        if (baseline == null || baseline.getReplicId() <= 0) {
            currentSCMState = new EleedSCMRevisionState();
            replicInfo = EleedServiceClient.GetLastReplicaInfo(stub, currentSCMState.getReplicCalendar());
        }
        else {
            currentSCMState = baseline;
            replicInfo = EleedServiceClient.GetNextReplicaInfo(stub, baseline.getReplicCalendar());
        }

        if (replicInfo != null) {
            currentSCMState.setReplicId(replicInfo.getID());
            currentSCMState.setReplicDateTime(replicInfo.getDateLoaded().getTimeInMillis());
            currentSCMState.setReplicInfo(new EleedReplicInfo(replicInfo));
        }
        return replicInfo != null;
    }
}

package org.jenkinsci.plugins.SCM;

import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.model.Job;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.scm.*;
import net.sf.json.JSONObject;
import org.jenkinsci.plugins.EleedServiceClient;
import org.jenkinsci.plugins.XmlWorker;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.StaplerRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;


public class EleedSCM extends SCM {

    private String adminServiceURL;
    private String login;
    private String password;
    private boolean isPollChanges = false;

    @DataBoundConstructor
    public EleedSCM(String adminServiceURL, String login, String password) {
        this.adminServiceURL = adminServiceURL;
        this.login = login;
        this.password = password;
        this.isPollChanges = false;
    }

    public String getAdminServiceURL() {
        return adminServiceURL;
    }

    @DataBoundSetter
    public void setAdminServiceURL(String adminServiceURL) {
        this.adminServiceURL = adminServiceURL;
    }

    public String getLogin() {
        return login;
    }

    @DataBoundSetter
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    @DataBoundSetter
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public SCMRevisionState calcRevisionsFromBuild(
            @Nonnull Run<?, ?> build,
            @Nullable FilePath workspace,
            @Nullable Launcher launcher,
            @Nonnull TaskListener listener)
            throws IOException, InterruptedException {

        listener.getLogger().println("calcRevisionsFromBuild");
        EleedSCMRevisionState result = null;

        EleedSCMRevisionState scmState = XmlWorker.readBaselineFromXml(workspace.getRemote());
        if (scmState == null)
            result = new EleedSCMRevisionState();

        if (isPollChanges)
            result = getSCMState(workspace, result);

        return result;
    }

    @Override
    public PollingResult compareRemoteRevisionWith(
            Job<?, ?> project,
            Launcher launcher,
            FilePath workspace,
            TaskListener listener,
            SCMRevisionState baseline)
            throws IOException, InterruptedException
    {
        launcher.getListener().getLogger().println("compareRemoteRevisionWith");

        return getReplicResult(workspace, baseline);
    }

    @Override
    public void checkout(
            Run<?,?> build,
            Launcher launcher,
            FilePath workspace,
            TaskListener listener,
            File changelogFile,
            SCMRevisionState baseline
    ) throws IOException, InterruptedException
    {

        listener.getLogger().println("checkout");

        if (baseline instanceof EleedSCMRevisionState) {
            XmlWorker.writeEleedSCMRevisionStateXml(baseline, build.getRootDir().getPath());
        }

        isPollChanges = true;
        if (build.getNumber() == 1)
            getReplicResult(workspace, baseline);
    }

    private PollingResult getReplicResult(FilePath workspace, SCMRevisionState baseline) throws RemoteException {

        EleedSCMRevisionState currentRemoteState = getSCMState(workspace, baseline);

        return new PollingResult(baseline, currentRemoteState,
                currentRemoteState.isHasChanges() ? PollingResult.Change.SIGNIFICANT : PollingResult.Change.NONE);
    }

    private EleedSCMRevisionState getSCMState(FilePath workspace, SCMRevisionState baseline) throws RemoteException {

        AdminReplicServiceStub stub = EleedServiceClient.GetReplicServiceStub(adminServiceURL, login, password);
        EleedSCMRevisionStateWorker currentRemoteState = new EleedSCMRevisionStateWorker();

        // Compare cached state with latest polled state
        boolean hasChanges = currentRemoteState.isNewerThan((EleedSCMRevisionState)baseline, stub);
        currentRemoteState.getCurrentSCMState().setHasChanges(hasChanges);
        if (hasChanges) {
            AdminReplicServiceStub.ReplicInfoDataContract replicInfo = currentRemoteState.getReplicInfo();
            if (replicInfo == null || replicInfo.getID() <= 0)
                throw new IllegalArgumentException("Не удалось загрузить информацию о реплике");
        }

        // Return a PollingResult to tell Jenkins whether to checkout and build or not
        XmlWorker.writeBaselineSerializationXml(currentRemoteState.getCurrentSCMState(), workspace.getRemote());
        return currentRemoteState.getCurrentSCMState();
    }


    @Override
    public ChangeLogParser createChangeLogParser() {
        return new EleedSCMChangeLogParser();
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl)super.getDescriptor();
    }

    @Extension
    public static final class DescriptorImpl extends SCMDescriptor<EleedSCM> {

        public DescriptorImpl() {
            super(EleedSCM.class, null);
            load();
        }

        @Override
        public SCM newInstance(StaplerRequest req, JSONObject formData) throws FormException {
            EleedSCM scm = (EleedSCM) super.newInstance(req, formData);
            return scm;
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
            save();
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Eleed SCM";
        }
    }
}

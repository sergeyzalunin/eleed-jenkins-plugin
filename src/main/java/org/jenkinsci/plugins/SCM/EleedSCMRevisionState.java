package org.jenkinsci.plugins.SCM;

import hudson.scm.SCMRevisionState;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.Calendar;

@Root()
public class  EleedSCMRevisionState extends SCMRevisionState {

    @Element()
    private long replicId;
    @Element()
    private long lastUpdate;
    @Element()
    private long replicDateTime;
    @Element()
    private boolean hasChanges;
    @Element()
    private EleedReplicInfo replicInfo;

    public EleedSCMRevisionState() {
        this(-1);
    }

    public EleedSCMRevisionState(long replicId) {
        this.setReplicId(replicId);
        this.setLastUpdate(Calendar.getInstance().getTimeInMillis());

        Calendar calendarInitTime = Calendar.getInstance();
        calendarInitTime.set(Calendar.YEAR, 1900);
        this.setReplicDateTime(calendarInitTime.getTimeInMillis());
    }

    public long getReplicId() {
        return replicId;
    }

    public void setReplicId(long replicId) {
        this.replicId = replicId;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getReplicDateTime() {
        return replicDateTime;
    }

    public Calendar getReplicCalendar() {
        Calendar result = Calendar.getInstance();
        result.setTimeInMillis(replicDateTime);
        return result;
    }

    public void setReplicDateTime(long replicDateTime) {
        this.replicDateTime = replicDateTime;
    }

    public boolean isHasChanges() {
        return hasChanges;
    }

    public void setHasChanges(boolean hasChanges) {
        this.hasChanges = hasChanges;
    }

    public EleedReplicInfo getReplicInfo() {
        return replicInfo;
    }

    public void setReplicInfo(EleedReplicInfo replicInfo) {
        this.replicInfo = replicInfo;
    }
}


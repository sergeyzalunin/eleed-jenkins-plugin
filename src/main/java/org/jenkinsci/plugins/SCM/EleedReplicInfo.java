package org.jenkinsci.plugins.SCM;

import org.simpleframework.xml.Element;
import java.util.Calendar;

public class EleedReplicInfo {

    @Element()
    private long iterationNumber;
    @Element()
    private long replicId;
    @Element()
    private String platformVersion;
    @Element()
    private Calendar dateLoaded;
    @Element()
    private String userName;
    @Element()
    private String comment;

    public EleedReplicInfo() {}

    public EleedReplicInfo(AdminReplicServiceStub.ReplicInfoDataContract replicInfo) {

        this.iterationNumber = replicInfo.getIterationNumber();
        this.replicId = replicInfo.getID();
        this.platformVersion = replicInfo.getPlatformVersion();
        this.dateLoaded = replicInfo.getDateLoaded();
        this.userName = replicInfo.getUserName();
        this.comment = replicInfo.getComment();
    }

    public long getIterationNumber() {
        return iterationNumber;
    }

    public void setIterationNumber(long iterationNumber) {
        this.iterationNumber = iterationNumber;
    }

    public long getReplicId() {
        return replicId;
    }

    public void setReplicId(long replicId) {
        this.replicId = replicId;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public Calendar getDateLoaded() {
        return dateLoaded;
    }

    public void setDateLoaded(Calendar dateLoaded) {
        this.dateLoaded = dateLoaded;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
